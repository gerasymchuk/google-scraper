import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Maksym on 26.08.2016.
 */
public class GoogleScraper {

    private static final String GOOGLE_SEARCH_BASE_URL = "%s/search?q=%s&start=%d";

    private static final Pattern patternDomainName;
    private static final String DOMAIN_NAME_PATTERN
            = "([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
    private static final String USER_AGENT = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
    public static final String CONFIG_PROPERTIES_FILE_PATH = "config.properties";
    private final Settings settings;

    static {
        patternDomainName = Pattern.compile(DOMAIN_NAME_PATTERN);
    }

    public static void main(String[] args) {
        GoogleScraper scraper = new GoogleScraper();
        String query = null;
        try {
            while(true) {
                System.out.println("Enter query or 'q' for exit:");
                query = new BufferedReader(new InputStreamReader(System.in)).readLine();
                if(query.toLowerCase().equals("q")) break;
                Set<String> result = scraper.getDataFromGoogle(query);
                scraper.save(query, result);
                result.forEach(System.out::println);
                System.out.println("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void save(String query, Set<String> result) {
        File file = null;
        try {
            file = new File(composeOutputFilePath(settings.getOutputPath(), query));
            file.getParentFile().mkdirs();
            try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))){
                for(String entry : result) {
                    writer.write(entry + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String composeOutputFilePath(String outputPath, String query) throws UnsupportedEncodingException {
        return outputPath + URLEncoder.encode(query, "UTF-8") +
                new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + ".csv";
    }

    public GoogleScraper() {
        super();
        settings = loadProperties();
    }

    private Settings loadProperties() {
        Properties prop = new Properties();
        Settings settings = Settings.DEFAULTS;
        try (InputStream input = new FileInputStream(CONFIG_PROPERTIES_FILE_PATH)){
            prop.load(input);
            settings.setBaseUrl(prop.getProperty("base_url", settings.getBaseUrl()));
            settings.setMaxPage(Integer.parseInt(prop.getProperty("max_page", settings.getMaxPage().toString())));
            settings.setOutputPath(prop.getProperty("output", settings.getOutputPath()));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return settings;
    }

    private Set<String> getDataFromGoogle(String query) {

        Set<String> result = new HashSet<>();

        for(int i= 0; i < settings.getMaxPage(); i++) {
            try {
                String request = String.format(GOOGLE_SEARCH_BASE_URL, settings.getBaseUrl(), URLEncoder.encode(query, "UTF-8"), i * 10);
                System.out.println(request + "\nSending request...");

                Document doc = Jsoup
                        .connect(request)
                        .userAgent(USER_AGENT)
                        .timeout(settings.getTimeout()).get();

                // get all links
                Elements links = doc.select("a[href]");
                for (Element link : links) {

                    String temp = link.attr("href");
                    if (temp.startsWith("/url?q=")) {
                        result.add(getDomainName(temp));
                    }
                }
                Thread.sleep(Math.round(Math.random()*(settings.getMaxInterval() - settings.getMinInterval())) + settings.getMinInterval());
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private String getDomainName(String url){
        String domainName = "";
        Matcher matcher = patternDomainName.matcher(url);
        if (matcher.find()) {
            domainName = matcher.group(0).toLowerCase().trim();
        }
        return domainName;

    }
}
