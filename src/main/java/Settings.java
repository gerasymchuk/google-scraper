/**
 * Created by Maksym on 29.08.2016.
 */
public class Settings {

    private String baseUrl;
    private int maxPage;
    private String outputPath;
    private Integer maxInterval;
    private Integer minInterval;
    private Integer timeout;

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setMaxPage(Integer maxPage) {
        this.maxPage = maxPage;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

    private static final String DEFAULT_BASE_URL = "http://www.google.com";
    private static final int DEFAULT_MAX_PAGE = 10;
    private static final String DEFAULT_OUTPUT_PATH = "output/";
    private static final Integer DEFAULT_MIN_INTERVAL = 5000;
    private static final Integer DEFAULT_MAX_INTERVAL = 15000;
    private static final Integer DEFAULT_TIMEOUT = 5000;

    public static final Settings DEFAULTS = new Settings(DEFAULT_BASE_URL, DEFAULT_MAX_PAGE, DEFAULT_OUTPUT_PATH);

    public Settings(String baseUrl, int maxPage, String outputPath) {
        this.baseUrl = baseUrl;
        this.maxPage = maxPage;
        this.outputPath = outputPath;
        this.maxInterval = DEFAULT_MAX_INTERVAL;
        this.minInterval = DEFAULT_MIN_INTERVAL;
        this.timeout = DEFAULT_TIMEOUT;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public Integer getMaxPage() {
        return maxPage;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public Integer getMaxInterval() {
        return maxInterval;
    }

    public Integer getMinInterval() {
        return minInterval;
    }

    public Integer getTimeout() {
        return timeout;
    }
}
